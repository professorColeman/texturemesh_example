#include "ofApp.h"


void ofApp::setup()
{
	ofBackground(80);

	webcam.setup(1280, 720);
	texture.allocate(1280, 720, 0);

	glm::vec3 v0 = { -500, -500, 0 };
	glm::vec3 v1 = { 500, -500, 0 };
	glm::vec3 v2 = { 500, 500, 0 };
	glm::vec3 v3 = { -500, 500, 0 };
	glm::vec3 v4 = { 1000, -500, 500 };
	glm::vec3 v5 = { 1000, 500, 500 };

	mesh.setMode(OF_PRIMITIVE_TRIANGLES);

	std::vector<glm::vec3> vertices =
	{
		v0, v1, v2, v3, v4, v5
	};

	// A note about float colors and how they correspond to 8-bit ofColors.
	// ofColor greenColor = ofColor(0, 255, 127);
	// ofFloatColor greenColor = ofFloatColor(0, 1, 0.5);


	std::vector<ofFloatColor> colors = {
		ofFloatColor::red,
		ofFloatColor::green,
		ofFloatColor::blue,
		ofFloatColor::white
	};

	std::vector<glm::vec2> textureCoordinates = {
		glm::vec2(0, 0),
		glm::vec2(1000, 0),
		glm::vec2(1000, texture.getHeight()),
		glm::vec2(0, texture.getHeight()),
		glm::vec2(texture.getWidth(), 0),
		glm::vec2(texture.getWidth(), texture.getHeight())
	};

	mesh.addVertices(vertices);
	mesh.addTexCoords(textureCoordinates);

	//mesh.addColors(colors);

	std::vector<ofIndexType> indices =
	{
		0, 1, 2,
		0, 2, 3,
		1, 5, 4,
		2, 5, 1
		
	};

	mesh.addIndices(indices);

}

void ofApp::update() {
	webcam.update();
	texture = webcam.getTexture();
}


void ofApp::draw()
{
	
	camera.begin();
	// Set texture as the "active" / "bound" texture.
	texture.bind();

	// Draw the mesh.  The texture coordinates added will refer to the "bound" texture.
	mesh.draw();

	// Disable the "active" / "bound" texture.
	texture.unbind();
	camera.end();


}